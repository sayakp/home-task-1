import React, { useEffect, useState } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import SearchBar from './components/SearchBar/SearchBar';
import Button from '../common/Button/Button';
import { mockedAuthorsList, mockedCoursesList } from '../contants';
import CourseCard from './components/CourseCard/CourseCard';
import CreateCourse from '../CreateCourse/CreateCourse';

function Courses() {
	const [courses, setCourses] = useState([]);
	const [authors, setAuthors] = useState([]);
	const [filteredCourses, setFilteredCourses] = useState([]);
	const [toggleAddCourse, setToggleAddCourse] = useState(false);

	useEffect(() => {
		// No needed, but using useEffect to simulate and API call to get the courses
		// Replace the author ids with author objects, leaving the id in case they are needed later
		const newCourses = mockedCoursesList.map((course) => {
			const newAuthors = course.authors.map((authorId) => {
				const author = mockedAuthorsList.find(
					(author) => author.id === authorId
				);
				return author ? author : { name: 'Unknown author', id: author.id };
			});
			return { ...course, authors: newAuthors };
		});

		setCourses(newCourses);
		setFilteredCourses(newCourses);
		setAuthors(mockedAuthorsList);
	}, []);

	useEffect(() => {
		// Reset the filter if new courses are added
		onSearchHandler('');
	}, [courses]);

	const onAddCurseHandler = () => {
		setToggleAddCourse((prevState) => !prevState);
	};

	const onSearchHandler = (filter) => {
		setFilteredCourses(
			courses.filter(
				(course) =>
					course.title.toLowerCase().includes(filter.toLowerCase()) ||
					course.id.toLowerCase().includes(filter.toLowerCase())
			)
		);
	};

	return (
		<Container>
			{toggleAddCourse && (
				<CreateCourse
					toggleCourse={onAddCurseHandler}
					setAuthors={setAuthors}
					setCourses={setCourses}
					authors={authors}
				/>
			)}
			{!toggleAddCourse && (
				<>
					<Row>
						<Col>
							<SearchBar onSearchHandler={onSearchHandler} />
						</Col>
						<Col className='text-end'>
							<Button
								onClick={onAddCurseHandler}
								variant='outline-primary'
								content='Add new course'
							/>
						</Col>
					</Row>
					<Row>
						{filteredCourses.map((course) => (
							<CourseCard {...course} key={course.id} />
						))}
					</Row>
				</>
			)}
		</Container>
	);
}

export default Courses;
