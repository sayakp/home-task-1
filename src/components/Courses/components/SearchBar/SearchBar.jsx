import React, { useState } from 'react';
import { InputGroup } from 'react-bootstrap';
import Input from '../../../common/Input/Input';
import Button from '../../../common/Button/Button';

function SearchBar({ onSearchHandler }) {
	const [searchInput, setSearchInput] = useState('');

	const onChangeHandler = (e) => {
		setSearchInput(e.target.value);
		if (!e.target.value) {
			onSearchHandler('');
		}
	};

	return (
		<InputGroup className='mb-3'>
			<Input
				label={'Search Course Name'}
				placeholder={'Enter course name or id...'}
				onChange={onChangeHandler}
				value={searchInput}
				onKeyPress={(e) => {
					if (e.key === 'Enter') {
						onSearchHandler(searchInput);
					}
				}}
			/>
			<Button
				variant='outline-secondary'
				content='Search'
				onClick={() => onSearchHandler(searchInput)}
			/>
		</InputGroup>
	);
}

export default SearchBar;
