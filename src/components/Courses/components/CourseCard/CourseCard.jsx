import React from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import './courseCard.css';
import Button from '../../../common/Button/Button';
import { minutesToCourseFormat } from '../../../helpers/dateGenerator';

function CourseCard({ title, description, creationDate, duration, authors }) {
	const authorNames = authors.map((author) => author.name);
	const joinedAuthors = authorNames.join(', ');

	const courseDuration = minutesToCourseFormat(duration);

	return (
		<Container className='my-3 course-card'>
			<Row>
				<Col md={8}>
					<h2 className={'m-3'}>{title}</h2>
					<p className={'m-3'}>{description}</p>
				</Col>
				<Col md={4}>
					<div>
						<p className='author-list'>
							<b>Authors:</b> {joinedAuthors}
						</p>
						<p>
							<b>Duration:</b> {courseDuration}
						</p>
						<p>
							<b>Created:</b> {creationDate}
						</p>
						<div className='text-center'>
							<Button
								variant='outline-primary'
								content='Show course'
								onClick={() => {}}
							/>
						</div>
					</div>
				</Col>
			</Row>
		</Container>
	);
}

export default CourseCard;
