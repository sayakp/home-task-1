import BootstrapButton from 'react-bootstrap/Button';

function Button({ content, ...rest }) {
	return <BootstrapButton {...rest}>{content}</BootstrapButton>;
}

export default Button;
