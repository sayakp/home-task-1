import React from 'react';
import { Form } from 'react-bootstrap';

function Input({ label, ...rest }) {
	return <Form.Control aria-label={label} {...rest} />;
}

export default Input;
