import React from 'react';
import { Nav, Navbar } from 'react-bootstrap';
import Logo from './components/Logo/Logo';
import Button from '../common/Button/Button';

function Header({ username }) {
	return (
		<Navbar
			collapseOnSelect
			expand='lg'
			bg='dark'
			variant='dark'
			className='mb-3'
		>
			<Navbar.Brand>
				<Logo />
			</Navbar.Brand>
			<Navbar.Collapse className={'justify-content-end'}>
				<Nav className='ms-auto'>
					<Nav.Link>{username}</Nav.Link>
					<Nav.Link>
						<Button
							variant='outline-info'
							content='logout'
							onClick={() => {}}
						/>
					</Nav.Link>
				</Nav>
			</Navbar.Collapse>
		</Navbar>
	);
}

export default Header;
