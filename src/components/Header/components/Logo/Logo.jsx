import React from 'react';
import './logo.css';

function Logo() {
	return <img src={'/images/Logo.png'} alt={'Logo'} className='logo' />;
}

export default Logo;
