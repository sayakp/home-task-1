import React, { useState } from 'react';
import { Col, Row, Form } from 'react-bootstrap';
import Input from '../common/Input/Input';
import Button from '../common/Button/Button';
import { generateDate, minutesToCourseFormat } from '../helpers/dateGenerator';
import { generateId } from '../helpers/idGenerator';
import './createCourse.css';

function CreateCourse({ toggleCourse, authors, setAuthors, setCourses }) {
	const [title, setTitle] = useState('');
	const [description, setDescription] = useState('');

	const [createAuthorName, setCreateAuthorName] = useState('');
	const [duration, setDuration] = useState('');
	const [addedAuthors, setAddedAuthors] = useState([]);

	const [validated, setValidated] = useState(false);

	const onAddAuthorToCourseHandler = (newAuthor) => {
		// Ensure author isn't already in the list
		if (!addedAuthors.find((author) => author.id === newAuthor.id)) {
			setAddedAuthors((prevState) => [...prevState, newAuthor]);
		}
	};

	const onRemoveAuthorHandler = (removedAuthor) => {
		setAddedAuthors((prevState) =>
			prevState.filter((author) => author.id !== removedAuthor.id)
		);
	};

	const onCreateAuthorHandler = () => {
		if (createAuthorName.length >= 2) {
			const newAuthor = {
				id: generateId(createAuthorName),
				name: createAuthorName,
			};
			setAuthors((prevState) => [...prevState, newAuthor]);
			setCreateAuthorName('');
		}
	};

	const onSubmitHandler = (event) => {
		const form = event.currentTarget;
		event.preventDefault();
		event.stopPropagation();
		if (form.checkValidity() === true) {
			const newCourse = {
				id: generateId(title),
				title,
				description,
				creationDate: generateDate(),
				duration,
				authors,
			};
			setCourses((prevState) => [...prevState, newCourse]);
			setTitle('');
			setDescription('');
			setDuration('');
			setAddedAuthors([]);
			toggleCourse();
		} else {
			alert('Please fill all fields');
		}

		setValidated(true);
	};

	return (
		<>
			<Form noValidate validated={validated} onSubmit={onSubmitHandler}>
				<Row>
					<Form.Group as={Col}>
						<Form.Label>Title</Form.Label>
						<Input
							value={title}
							onChange={(e) => setTitle(e.target.value)}
							placeholder={'Enter title...'}
							label={'Course Title'}
							required
						/>
					</Form.Group>
					<Col className='text-end'>
						<Button
							variant='outline-success'
							content='Create Course'
							type='submit'
						/>
					</Col>
				</Row>
				<Row className='bottom-row'>
					<Form.Group as={Col}>
						<Form.Label>Description</Form.Label>
						<Input
							value={description}
							onChange={(e) => setDescription(e.target.value)}
							placeholder={'Enter description'}
							label={'Course description'}
							minLength={2}
							as='textarea'
							rows={3}
							required
						/>
						<Form.Control.Feedback type='invalid'>
							Minimum description length is 2
						</Form.Control.Feedback>
					</Form.Group>
				</Row>
				<Row className='mt-5'>
					<Col>
						<Row>
							<Row className='text-center'>
								<h5>Add Author</h5>
							</Row>
							<Row>
								<Form.Label>Author name</Form.Label>
								<Input
									value={createAuthorName}
									onChange={(e) => setCreateAuthorName(e.target.value)}
									placeholder={'Enter author name...'}
									label={'New Author Name'}
									className='prevent-validation'
								/>
							</Row>
							<Row className='justify-content-center'>
								<Col md={6} className='mt-2 text-center'>
									<Button
										onClick={onCreateAuthorHandler}
										variant={'outline-primary'}
										content='Create Author'
									/>
								</Col>
							</Row>
						</Row>
						<Row className='mt-5'>
							<Row className='text-center'>
								<h5>Duration</h5>
							</Row>
							<Form.Group as={Row}>
								<Form.Label>Duration</Form.Label>
								<Input
									value={duration}
									onChange={(e) => {
										setDuration(e.target.value);
									}}
									placeholder={'Enter duration in minutes...'}
									label={'Course duration in minutes'}
									type='number'
									min='1'
									required
									onKeyPress={(e) => {
										// Prevent adding negative numbers
										if (e.key === '-') {
											e.preventDefault();
										}
									}}
								/>
								<Form.Control.Feedback type='invalid'>
									Minimum duration is 1 minute
								</Form.Control.Feedback>
								<div className='course-duration-container'>
									<p>
										Duration: <b>{minutesToCourseFormat(duration)}</b>
									</p>
								</div>
							</Form.Group>
						</Row>
					</Col>
					<Col>
						<Row>
							<Row className='text-center mb-3'>
								<h5>Authors</h5>
							</Row>
							{authors.map((author) => {
								const authorIsAdded = addedAuthors.find(
									(addedAuthor) => addedAuthor.id === author.id
								);
								if (!authorIsAdded) {
									return (
										<Row className='py-1' key={author.id}>
											<Col>{author.name}</Col>
											<Col>
												<Button
													variant='outline-success'
													onClick={() => {
														onAddAuthorToCourseHandler(author);
													}}
													content='Add Author'
												/>
											</Col>
										</Row>
									);
								}
								return null;
							})}
						</Row>
						<Row className={'mt-4'}>
							<Row className='text-center'>
								<h5>Course Authors</h5>
							</Row>
							<Row className='text-center mt-3'>
								{addedAuthors.length === 0 && <p>Author list is empty</p>}
								{addedAuthors.map((author) => (
									<Row key={author.id}>
										<Col>
											<p>{author.name}</p>
										</Col>
										<Col>
											<Button
												variant='outline-danger'
												onClick={() => {
													onRemoveAuthorHandler(author);
												}}
												content='Remove Author'
											/>
										</Col>
									</Row>
								))}
							</Row>
						</Row>
					</Col>
				</Row>
			</Form>
		</>
	);
}

export default CreateCourse;
