export const generateId = (name) => {
	const random = Math.floor(Math.random() * 1000);
	const date = new Date().getTime();
	return `${name}_${date}_${random}`;
};
