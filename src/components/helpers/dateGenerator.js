// Converts minutes to Course Format (XX:XX hours)
export const minutesToCourseFormat = (minutes) => {
	const hours = Math.floor(minutes / 60);
	const remainingMinutes = minutes % 60;
	return `${hours.toString().padStart(2, '0')}:${remainingMinutes
		.toString()
		.padStart(2, '0')} hours`;
};

export const generateDate = () => {
	const today = new Date();
	const options = { month: 'numeric', day: 'numeric', year: 'numeric' };
	return today.toLocaleDateString('en-US', options);
};
