import Header from './components/Header/Header';
import React from 'react';
import Courses from './components/Courses/Courses';

function App() {
	return (
		<>
			<Header username={'Daniel'} />
			<Courses />
		</>
	);
}

export default App;
